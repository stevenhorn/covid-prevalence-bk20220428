0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 122 log-likelihood matrix

         Estimate       SE
elpd_loo  -874.76    15.51
p_loo        9.77        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      119   97.5%
 (0.5, 0.7]   (ok)          3    2.5%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.174    0.010    0.162     0.192      0.004    0.003       5.0     5.0       5.0      14.0   2.63
pu          0.708    0.007    0.700     0.717      0.003    0.003       4.0     4.0       5.0      18.0   3.16
mu          0.111    0.007    0.101     0.119      0.004    0.003       4.0     4.0       6.0      29.0   2.21
mus         0.209    0.007    0.194     0.216      0.003    0.002       4.0     4.0       5.0      12.0   2.44
gamma       0.435    0.056    0.352     0.513      0.027    0.020       4.0     4.0       5.0      21.0   3.14
Is_begin  658.191  272.674  199.941  1125.520    126.886   96.091       5.0     5.0       5.0      12.0   3.89
Ia_begin  220.009  351.677    2.344   925.958    163.984  124.223       5.0     5.0       5.0      12.0   3.43
E_begin   336.157  298.921    0.635   909.327    137.409  103.873       5.0     5.0       5.0      12.0   2.76