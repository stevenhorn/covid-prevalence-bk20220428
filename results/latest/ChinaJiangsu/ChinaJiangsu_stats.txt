0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 113 log-likelihood matrix

         Estimate       SE
elpd_loo  -309.60    22.19
p_loo       10.75        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       99   87.6%
 (0.5, 0.7]   (ok)          7    6.2%
   (0.7, 1]   (bad)         3    2.7%
   (1, Inf)   (very bad)    4    3.5%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.249   0.048   0.184    0.349      0.020    0.015       6.0     6.0       6.0      12.0   1.86
pu         0.840   0.014   0.822    0.869      0.006    0.005       5.0     5.0       6.0      12.0   2.09
mu         0.109   0.008   0.098    0.122      0.004    0.003       5.0     5.0       5.0      19.0   3.15
mus        0.169   0.027   0.123    0.208      0.010    0.007       8.0     8.0       8.0      29.0   1.52
gamma      0.365   0.068   0.276    0.468      0.023    0.017       8.0     8.0       9.0      36.0   1.43
Is_begin   5.896   5.965   0.322   16.743      2.448    1.824       6.0     6.0       6.0      32.0   1.94
Ia_begin  30.675  25.195   2.745   81.682      8.952    6.579       8.0     8.0       9.0      43.0   1.39
E_begin   21.455  16.375   1.832   49.969      5.395    3.943       9.0     9.0      11.0      14.0   1.34