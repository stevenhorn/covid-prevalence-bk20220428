0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 181 log-likelihood matrix

         Estimate       SE
elpd_loo  -523.81    17.56
p_loo       12.29        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      178   98.3%
 (0.5, 0.7]   (ok)          1    0.6%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    1    0.6%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.214    0.020   0.184    0.249      0.009    0.007       5.0     5.0       5.0      12.0   3.28
pu          0.792    0.004   0.786    0.800      0.002    0.002       4.0     4.0       5.0      16.0   3.30
mu          0.108    0.001   0.107    0.109      0.000    0.000       5.0     5.0       5.0      12.0   3.22
mus         0.187    0.014   0.164    0.203      0.007    0.005       4.0     4.0       5.0      12.0   3.09
gamma       0.370    0.025   0.327    0.394      0.012    0.009       4.0     4.0       5.0      33.0   2.76
Is_begin   40.730   29.025   2.160   86.982     12.689    9.526       5.0     5.0       6.0      26.0   2.16
Ia_begin  392.195  310.375  16.040  943.454    138.752  104.478       5.0     5.0       5.0      16.0   2.71
E_begin   235.171  138.070  12.028  434.360     64.060   48.492       5.0     5.0       5.0      13.0   3.16