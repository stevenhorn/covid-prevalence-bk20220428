0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 109 log-likelihood matrix

         Estimate       SE
elpd_loo  -768.92    14.34
p_loo       23.26        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      108   99.1%
 (0.5, 0.7]   (ok)          1    0.9%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

               mean         sd    hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa            0.327      0.015     0.302      0.344      0.007     0.006       4.0     4.0       5.0      16.0   3.73
pu            0.896      0.001     0.894      0.897      0.000     0.000       5.0     5.0       5.0      19.0   2.45
mu            0.130      0.002     0.127      0.132      0.001     0.001       4.0     4.0       5.0      12.0   3.13
mus           0.195      0.054     0.152      0.288      0.026     0.020       4.0     4.0       5.0      12.0   3.25
gamma         0.323      0.084     0.230      0.414      0.040     0.031       4.0     4.0       5.0      13.0   3.04
Is_begin   6941.707   1948.263  4101.676   9652.536    920.829   699.990       4.0     4.0       5.0      13.0   3.57
Ia_begin  16505.934  20761.000  3672.076  53945.372   9953.775  7572.949       4.0     4.0       5.0      12.0   2.38
E_begin    3058.917   3600.573   294.892   9280.328   1726.066  1313.186       4.0     4.0       5.0      12.0   3.55