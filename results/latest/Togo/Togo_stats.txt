0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 149 log-likelihood matrix

         Estimate       SE
elpd_loo  -668.91    18.24
p_loo        9.88        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      139   93.3%
 (0.5, 0.7]   (ok)          6    4.0%
   (0.7, 1]   (bad)         3    2.0%
   (1, Inf)   (very bad)    1    0.7%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.260   0.050   0.164    0.328      0.023    0.017       5.0     5.0       5.0      14.0   2.63
pu         0.718   0.012   0.703    0.740      0.006    0.004       4.0     4.0       5.0      15.0   3.59
mu         0.249   0.010   0.231    0.257      0.005    0.004       4.0     4.0       5.0      14.0   2.47
mus        0.359   0.011   0.345    0.376      0.005    0.004       4.0     4.0       5.0      22.0   2.68
gamma      0.753   0.068   0.640    0.855      0.032    0.024       4.0     4.0       5.0      12.0   3.44
Is_begin  51.683  46.706   5.697  126.698     22.185   16.854       4.0     4.0       5.0      15.0   3.28
Ia_begin  78.473  34.795  28.350  126.658     16.188   12.259       5.0     5.0       5.0      16.0   2.68
E_begin   46.455  42.965   0.847  106.050     20.471   15.559       4.0     4.0       5.0      17.0   3.35