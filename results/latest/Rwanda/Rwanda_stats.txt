0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 169 log-likelihood matrix

         Estimate       SE
elpd_loo  -900.24    22.18
p_loo       24.20        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      168   99.4%
 (0.5, 0.7]   (ok)          1    0.6%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

              mean        sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.242     0.038   0.172     0.287      0.018    0.014       4.0     4.0       5.0      12.0   2.82
pu           0.747     0.055   0.701     0.842      0.027    0.020       4.0     4.0       5.0      16.0   3.34
mu           0.211     0.004   0.206     0.216      0.002    0.001       4.0     4.0       5.0      12.0   4.09
mus          0.329     0.009   0.319     0.346      0.004    0.003       6.0     6.0       6.0      19.0   2.34
gamma        0.786     0.140   0.594     0.945      0.067    0.051       4.0     4.0       5.0      12.0   3.15
Is_begin  1426.389  2419.190   0.968  5667.075   1160.160  882.699       4.0     4.0       5.0      14.0   3.62
Ia_begin   124.227   147.079   0.349   370.362     70.494   53.652       4.0     4.0       5.0      41.0   2.99
E_begin    137.650   232.146   0.250   540.983    111.329   84.704       4.0     4.0       5.0      12.0   2.65