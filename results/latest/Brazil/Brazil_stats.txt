0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 83 log-likelihood matrix

         Estimate       SE
elpd_loo -1027.27    40.15
p_loo       73.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       80   96.4%
 (0.5, 0.7]   (ok)          2    2.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    1.2%

                 mean           sd   hdi_3%      hdi_97%    mcse_mean      mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa              0.345        0.004    0.338        0.349        0.002        0.001       4.0     4.0       5.0      12.0   3.34
pu              0.898        0.003    0.893        0.900        0.001        0.001       4.0     4.0       5.0      12.0   3.57
mu              0.080        0.002    0.078        0.083        0.001        0.001       4.0     4.0       5.0      41.0   3.22
mus             0.300        0.065    0.242        0.409        0.031        0.024       4.0     4.0       5.0      12.0   2.69
gamma           0.745        0.185    0.547        0.938        0.089        0.068       4.0     4.0       5.0      14.0   3.29
Is_begin    52345.722    44105.394  216.204   123097.525    20920.371    15889.419       4.0     4.0       5.0      23.0   3.09
Ia_begin  2182171.080  3270616.349  152.920  8011915.883  1567629.125  1192615.201       4.0     4.0       5.0      12.0   3.92
E_begin    373390.878   327569.632  932.937   849602.145   154971.645   117656.206       4.0     4.0       5.0      19.0   2.99