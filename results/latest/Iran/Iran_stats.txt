0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 95 log-likelihood matrix

         Estimate       SE
elpd_loo -1157.48    76.39
p_loo       61.98        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       92   96.8%
 (0.5, 0.7]   (ok)          2    2.1%
   (0.7, 1]   (bad)         1    1.1%
   (1, Inf)   (very bad)    0    0.0%

              mean        sd   hdi_3%   hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.347     0.001    0.345     0.350      0.001     0.001       4.0     4.0       5.0      12.0   3.09
pu           0.898     0.001    0.897     0.900      0.001     0.000       4.0     4.0       5.0      23.0   3.33
mu           0.063     0.004    0.059     0.070      0.002     0.002       4.0     4.0       5.0      15.0   3.38
mus          0.492     0.081    0.396     0.623      0.039     0.030       4.0     4.0       5.0      13.0   3.49
gamma        0.976     0.034    0.935     1.012      0.016     0.013       4.0     4.0       5.0      12.0   3.70
Is_begin   789.092   955.519   11.902  2468.749    457.201   347.732       4.0     4.0       5.0      31.0   2.98
Ia_begin  1263.174  1686.908    0.096  4234.984    808.847   615.388       4.0     4.0       5.0      12.0   3.46
E_begin   2748.807  2948.068  701.983  8265.101   1412.953  1074.932       4.0     4.0       5.0      13.0   3.24