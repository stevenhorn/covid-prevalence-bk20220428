0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 136 log-likelihood matrix

         Estimate       SE
elpd_loo -1184.01    91.77
p_loo       26.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      130   95.6%
 (0.5, 0.7]   (ok)          3    2.2%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    3    2.2%

               mean         sd  hdi_3%     hdi_97%  mcse_mean    mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa            0.346      0.003   0.343       0.350      0.001      0.001       4.0     4.0       5.0      19.0   2.67
pu            0.899      0.001   0.897       0.900      0.000      0.000       4.0     4.0       5.0      15.0   3.22
mu            0.200      0.004   0.195       0.206      0.002      0.001       5.0     5.0       5.0      26.0   2.41
mus           0.293      0.029   0.258       0.348      0.014      0.011       4.0     4.0       5.0      17.0   4.21
gamma         0.805      0.091   0.665       0.966      0.042      0.032       5.0     5.0       5.0      16.0   2.70
Is_begin     49.099     39.288   8.056     128.293     17.633     13.285       5.0     5.0       5.0      19.0   2.98
Ia_begin   8367.899  13211.122  21.349   36658.669   6052.015   4572.667       5.0     5.0       5.0      18.0   3.66
E_begin   48127.716  53873.849   7.992  140308.060  23999.250  18062.394       5.0     5.0       5.0      14.0   2.59