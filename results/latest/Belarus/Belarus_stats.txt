0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 131 log-likelihood matrix

         Estimate       SE
elpd_loo -1137.58    23.12
p_loo       36.71        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      120   91.6%
 (0.5, 0.7]   (ok)          5    3.8%
   (0.7, 1]   (bad)         3    2.3%
   (1, Inf)   (very bad)    3    2.3%

             mean       sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.347    0.002   0.344     0.350      0.001    0.001       5.0     5.0       5.0      12.0   2.64
pu          0.897    0.002   0.895     0.900      0.001    0.001       4.0     4.0       5.0      13.0   3.86
mu          0.104    0.004   0.097     0.109      0.002    0.001       4.0     4.0       5.0      12.0   3.22
mus         0.303    0.052   0.243     0.382      0.025    0.019       4.0     4.0       5.0      12.0   4.06
gamma       0.672    0.144   0.447     0.814      0.069    0.052       4.0     4.0       5.0      25.0   2.37
Is_begin   45.321   33.423   4.400    91.204     15.210   11.482       5.0     5.0       5.0      12.0   2.40
Ia_begin   44.990   37.458   0.491   128.149     16.466   12.370       5.0     5.0       5.0      12.0   2.71
E_begin   598.704  699.216   2.684  1773.862    335.178  255.000       4.0     4.0       5.0      12.0   3.21