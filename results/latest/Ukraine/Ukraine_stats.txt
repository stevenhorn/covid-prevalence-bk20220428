0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 59 log-likelihood matrix

         Estimate       SE
elpd_loo  -628.59    20.70
p_loo       30.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       58   98.3%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    1.7%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.340    0.004   0.334    0.348      0.002    0.001       5.0     5.0       6.0      14.0   2.16
pu          0.896    0.002   0.894    0.898      0.001    0.001       4.0     4.0       5.0      14.0   3.55
mu          0.097    0.006   0.088    0.104      0.003    0.002       4.0     4.0       5.0      13.0   3.07
mus         0.259    0.013   0.234    0.272      0.006    0.005       5.0     5.0       6.0      28.0   2.04
gamma       0.665    0.171   0.367    0.821      0.082    0.062       4.0     4.0       5.0      15.0   2.52
Is_begin   99.268  118.969  16.706  323.110     56.550   42.965       4.0     4.0       5.0      14.0   2.81
Ia_begin  100.021  161.088   0.378  415.650     74.106   56.025       5.0     5.0       5.0      15.0   2.56
E_begin   106.281  129.640   4.721  340.704     61.965   47.120       4.0     4.0       5.0      12.0   3.53