0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 153 log-likelihood matrix

         Estimate       SE
elpd_loo  -899.53    19.46
p_loo       12.62        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      152   99.3%
 (0.5, 0.7]   (ok)          1    0.7%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd   hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.211    0.049    0.153    0.264      0.023    0.018       4.0     4.0       5.0      21.0   2.68
pu          0.752    0.021    0.734    0.789      0.010    0.008       4.0     4.0       5.0      17.0   3.02
mu          0.154    0.006    0.145    0.160      0.003    0.002       4.0     4.0       5.0      23.0   2.89
mus         0.241    0.017    0.220    0.263      0.008    0.006       4.0     4.0       5.0      20.0   3.11
gamma       0.465    0.100    0.301    0.583      0.048    0.036       4.0     4.0       5.0      13.0   3.97
Is_begin  499.182  238.252  204.915  809.646    112.810   85.658       4.0     4.0       5.0      12.0   2.27
Ia_begin  337.502  204.543  124.481  758.139     92.225   69.525       5.0     5.0       5.0      14.0   2.85
E_begin   244.848  192.029   20.564  544.018     85.109   64.012       5.0     5.0       6.0      32.0   2.05