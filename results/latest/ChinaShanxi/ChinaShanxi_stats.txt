0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 151 log-likelihood matrix

         Estimate       SE
elpd_loo  -132.37    26.71
p_loo       14.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      148   98.0%
 (0.5, 0.7]   (ok)          1    0.7%
   (0.7, 1]   (bad)         1    0.7%
   (1, Inf)   (very bad)    1    0.7%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.281   0.033   0.235    0.341      0.011    0.009       8.0     8.0      10.0      38.0   1.47
pu         0.877   0.013   0.855    0.892      0.006    0.004       5.0     5.0       5.0      42.0   2.28
mu         0.146   0.020   0.110    0.177      0.007    0.005       7.0     7.0       7.0      28.0   1.67
mus        0.185   0.024   0.142    0.218      0.010    0.007       6.0     6.0       7.0      37.0   1.67
gamma      0.418   0.048   0.356    0.500      0.017    0.012       9.0     9.0       9.0      29.0   1.44
Is_begin   3.981   2.153   0.436    7.890      0.588    0.425      13.0    13.0      11.0      17.0   1.36
Ia_begin  30.257  26.038   1.303   76.270      6.309    4.540      17.0    17.0      13.0      26.0   1.24
E_begin   11.539   8.830   1.000   31.333      3.098    2.274       8.0     8.0       6.0      18.0   1.88