0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 79 log-likelihood matrix

         Estimate       SE
elpd_loo  -663.73    18.37
p_loo       33.05        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       77   97.5%
 (0.5, 0.7]   (ok)          2    2.5%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

              mean         sd  hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.293      0.057   0.194      0.347      0.028     0.021       4.0     4.0       5.0      25.0   3.45
pu           0.882      0.011   0.872      0.900      0.005     0.004       4.0     4.0       6.0      16.0   2.19
mu           0.152      0.007   0.140      0.159      0.003     0.002       4.0     4.0       5.0      12.0   3.65
mus          0.246      0.062   0.163      0.342      0.029     0.022       4.0     4.0       5.0      15.0   3.45
gamma        0.564      0.235   0.206      0.839      0.113     0.086       4.0     4.0       5.0      12.0   3.61
Is_begin  9663.174  16768.771   0.286  40548.916   8026.917  6105.414       4.0     4.0       5.0      16.0   3.15
Ia_begin  6414.304  10903.577  13.809  25946.452   5227.190  3976.848       4.0     4.0       5.0      18.0   3.39
E_begin   2120.752   3633.678   3.355   8662.279   1740.374  1323.880       4.0     4.0       5.0      37.0   3.11