0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 118 log-likelihood matrix

         Estimate       SE
elpd_loo  -429.80    15.86
p_loo       10.46        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      113   95.8%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         2    1.7%
   (1, Inf)   (very bad)    1    0.8%

             mean       sd   hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.263    0.036    0.218     0.326      0.016    0.012       5.0     5.0       6.0      19.0   2.12
pu          0.888    0.006    0.874     0.896      0.003    0.002       6.0     6.0       6.0      25.0   1.82
mu          0.124    0.008    0.109     0.134      0.004    0.003       5.0     5.0       5.0      13.0   2.36
mus         0.160    0.023    0.127     0.201      0.011    0.008       5.0     5.0       5.0      20.0   2.57
gamma       0.428    0.047    0.362     0.516      0.019    0.014       6.0     6.0       6.0      25.0   1.85
Is_begin  148.732   86.970   23.572   281.196     33.816   25.053       7.0     7.0       7.0      32.0   1.62
Ia_begin  736.468  377.251  150.946  1467.130     65.979   47.077      33.0    33.0      31.0      49.0   1.19
E_begin   708.835  487.043   10.763  1642.028    189.140  140.110       7.0     7.0       7.0      43.0   1.68