0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 133 log-likelihood matrix

         Estimate       SE
elpd_loo  -499.77    15.55
p_loo        7.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      130   97.7%
 (0.5, 0.7]   (ok)          2    1.5%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.253   0.051   0.178    0.341      0.024    0.018       5.0     5.0       5.0      15.0   3.13
pu         0.759   0.029   0.721    0.796      0.013    0.010       5.0     5.0       6.0      29.0   2.15
mu         0.135   0.006   0.128    0.147      0.003    0.002       5.0     5.0       5.0      23.0   2.71
mus        0.194   0.019   0.169    0.229      0.009    0.007       5.0     5.0       5.0      12.0   2.75
gamma      0.578   0.064   0.446    0.680      0.029    0.022       5.0     5.0       5.0      12.0   2.56
Is_begin   9.919  11.334   0.312   30.330      3.972    2.916       8.0     8.0       7.0      21.0   1.71
Ia_begin  73.843  86.301   0.238  241.265     30.322   22.264       8.0     8.0       6.0      12.0   2.09
E_begin   32.083  32.870   0.062   97.667     15.371   11.649       5.0     5.0       5.0      12.0   3.19