0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 65 log-likelihood matrix

         Estimate       SE
elpd_loo  -491.44    12.25
p_loo       12.69        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       64   98.5%
 (0.5, 0.7]   (ok)          1    1.5%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.247    0.044   0.163     0.299      0.019    0.014       5.0     5.0       7.0      14.0   1.77
pu          0.786    0.053   0.715     0.850      0.025    0.019       4.0     4.0       5.0      13.0   2.61
mu          0.137    0.002   0.134     0.140      0.001    0.001       5.0     5.0       5.0      12.0   2.45
mus         0.229    0.032   0.172     0.262      0.015    0.012       4.0     4.0       5.0      18.0   3.14
gamma       0.478    0.067   0.363     0.553      0.032    0.024       4.0     4.0       5.0      12.0   3.35
Is_begin  635.793  873.266   6.943  2182.407    411.049  311.829       5.0     5.0       6.0      18.0   2.20
Ia_begin  349.929  265.635  61.315   942.836    123.366   93.398       5.0     5.0       5.0      12.0   3.03
E_begin   542.367  687.161   1.863  1758.024    319.918  242.291       5.0     5.0       5.0      12.0   3.54