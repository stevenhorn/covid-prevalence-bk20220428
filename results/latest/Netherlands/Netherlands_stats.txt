0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 109 log-likelihood matrix

         Estimate       SE
elpd_loo -1210.51    49.81
p_loo       56.51        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      102   93.6%
 (0.5, 0.7]   (ok)          2    1.8%
   (0.7, 1]   (bad)         2    1.8%
   (1, Inf)   (very bad)    3    2.8%

            mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.156    0.005   0.151    0.164      0.002    0.002       5.0     5.0       6.0      15.0   2.32
pu         0.702    0.001   0.700    0.704      0.000    0.000       4.0     4.0       5.0      13.0   3.74
mu         0.139    0.004   0.133    0.146      0.002    0.001       5.0     5.0       5.0      12.0   3.51
mus        0.234    0.013   0.211    0.249      0.006    0.005       5.0     5.0       5.0      18.0   2.54
gamma      0.695    0.036   0.649    0.755      0.017    0.013       5.0     5.0       6.0      24.0   2.13
Is_begin  28.466   42.978   0.746  143.653     19.394   14.622       5.0     5.0       5.0      13.0   3.43
Ia_begin  68.520  103.100   0.102  250.342     43.170   32.239       6.0     6.0       5.0      12.0   3.19
E_begin   25.260   28.542   0.576   82.183     12.752    9.601       5.0     5.0       5.0      14.0   2.95