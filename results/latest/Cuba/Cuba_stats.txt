0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 161 log-likelihood matrix

         Estimate       SE
elpd_loo -1431.58    49.66
p_loo      108.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      159   98.8%
 (0.5, 0.7]   (ok)          1    0.6%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.6%

                mean          sd  hdi_3%      hdi_97%   mcse_mean     mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa             0.340       0.009   0.326        0.349       0.004       0.003       4.0     4.0       5.0      14.0   3.19
pu             0.874       0.030   0.825        0.900       0.014       0.011       4.0     4.0       5.0      12.0   3.81
mu             0.038       0.004   0.033        0.044       0.002       0.002       4.0     4.0       5.0      13.0   3.50
mus            0.305       0.060   0.210        0.357       0.029       0.022       4.0     4.0       5.0      43.0   2.42
gamma          0.789       0.253   0.350        0.979       0.121       0.092       4.0     4.0       5.0      17.0   3.02
Is_begin   17196.030   34170.622   3.596    89655.974   15677.469   11847.893       5.0     5.0       5.0      12.0   3.63
Ia_begin  468423.558  811309.197   0.607  1895650.349  389014.212  295970.899       4.0     4.0       5.0      15.0   3.30
E_begin    11163.699   21061.044   0.704    58780.862    9726.206    7357.365       5.0     5.0       5.0      12.0   4.07