0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 117 log-likelihood matrix

         Estimate       SE
elpd_loo -1297.78    36.93
p_loo       82.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      111   94.9%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         3    2.6%
   (1, Inf)   (very bad)    1    0.9%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.261    0.035   0.215    0.313      0.017    0.013       4.0     4.0       5.0      13.0   3.33
pu          0.801    0.032   0.760    0.850      0.015    0.012       4.0     4.0       5.0      18.0   3.08
mu          0.123    0.003   0.120    0.128      0.002    0.001       4.0     4.0       5.0      12.0   3.77
mus         0.624    0.085   0.492    0.710      0.041    0.031       4.0     4.0       5.0      12.0   2.60
gamma       0.958    0.048   0.871    0.996      0.023    0.017       4.0     4.0       5.0      12.0   3.38
Is_begin   41.907   70.457   0.312  172.720     33.776   25.697       4.0     4.0       5.0      12.0   3.30
Ia_begin   42.492   64.381   0.881  158.687     30.864   23.481       4.0     4.0       5.0      12.0   4.01
E_begin   265.442  357.434   2.116  909.822    171.076  130.121       4.0     4.0       5.0      20.0   3.47