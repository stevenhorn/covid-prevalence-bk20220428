0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 123 log-likelihood matrix

         Estimate       SE
elpd_loo  -649.18    14.71
p_loo        6.36        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      123  100.0%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

               mean         sd  hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa            0.270      0.040   0.201      0.313      0.019     0.014       4.0     4.0       5.0      12.0   3.38
pu            0.856      0.013   0.834      0.868      0.006     0.005       4.0     4.0       5.0      12.0   3.01
mu            0.163      0.001   0.161      0.165      0.001     0.000       4.0     4.0       5.0      21.0   2.83
mus           0.179      0.009   0.164      0.189      0.004     0.003       4.0     4.0       5.0      22.0   3.11
gamma         0.459      0.019   0.433      0.486      0.009     0.007       4.0     4.0       5.0      16.0   3.82
Is_begin    612.771    441.170  74.449   1474.178    189.071   141.591       5.0     5.0       5.0      14.0   2.59
Ia_begin  14031.382  14193.831   4.442  34653.813   6615.630  5011.235       5.0     5.0       5.0      19.0   2.51
E_begin    4899.705   4623.661  88.129  12367.803   2186.649  1660.036       4.0     4.0       5.0      12.0   2.64