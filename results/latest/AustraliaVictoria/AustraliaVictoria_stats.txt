0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 121 log-likelihood matrix

         Estimate       SE
elpd_loo -1063.03    30.60
p_loo       19.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      119   98.3%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    1.7%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.244   0.043   0.189    0.317      0.020    0.015       5.0     5.0       5.0      17.0   2.84
pu         0.744   0.009   0.735    0.762      0.004    0.003       4.0     4.0       5.0      13.0   3.34
mu         0.120   0.003   0.116    0.124      0.001    0.001       4.0     4.0       5.0      12.0   4.00
mus        0.170   0.013   0.159    0.194      0.006    0.005       4.0     4.0       5.0      16.0   2.53
gamma      0.548   0.021   0.525    0.582      0.010    0.008       4.0     4.0       5.0      20.0   3.02
Is_begin   8.361   9.861   0.232   25.770      4.666    3.542       4.0     4.0       5.0      13.0   3.52
Ia_begin  32.002  19.313   1.749   57.282      8.856    6.692       5.0     5.0       5.0      12.0   2.26
E_begin   14.543   7.677   3.142   27.826      2.904    2.146       7.0     7.0       7.0      18.0   1.61