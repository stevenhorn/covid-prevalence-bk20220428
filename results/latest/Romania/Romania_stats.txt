0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 106 log-likelihood matrix

         Estimate       SE
elpd_loo -1029.44    25.10
p_loo       56.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      104   98.1%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.9%
   (1, Inf)   (very bad)    1    0.9%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.316    0.006   0.306    0.322      0.003    0.002       5.0     5.0       6.0      19.0   2.16
pu          0.851    0.012   0.831    0.863      0.006    0.004       4.0     4.0       5.0      12.0   3.40
mu          0.089    0.006   0.082    0.096      0.003    0.002       4.0     4.0       5.0      12.0   3.69
mus         0.228    0.049   0.162    0.298      0.023    0.018       4.0     4.0       5.0      18.0   3.48
gamma       0.629    0.058   0.561    0.725      0.028    0.021       4.0     4.0       5.0      12.0   3.18
Is_begin   17.655   20.133   1.503   53.462      9.611    7.311       4.0     4.0       5.0      12.0   3.83
Ia_begin   30.975   25.812   1.129   58.896     12.368    9.409       4.0     4.0       5.0      15.0   2.70
E_begin   127.540  175.722   2.194  430.992     84.264   64.111       4.0     4.0       5.0      13.0   3.56