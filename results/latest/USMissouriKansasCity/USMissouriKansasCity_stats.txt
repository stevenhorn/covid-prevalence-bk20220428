0 Divergences 
Failed validation 
2022-03-20 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 112 log-likelihood matrix

         Estimate       SE
elpd_loo  -731.11    19.96
p_loo       27.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      107   95.5%
 (0.5, 0.7]   (ok)          3    2.7%
   (0.7, 1]   (bad)         2    1.8%
   (1, Inf)   (very bad)    0    0.0%

              mean       sd   hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.265    0.045    0.216     0.334      0.022    0.017       4.0     4.0       5.0      43.0   2.87
pu           0.841    0.022    0.809     0.880      0.010    0.008       5.0     5.0       5.0      25.0   2.73
mu           0.151    0.006    0.141     0.159      0.003    0.002       4.0     4.0       5.0      14.0   3.49
mus          0.213    0.027    0.177     0.254      0.013    0.010       4.0     4.0       5.0      12.0   3.95
gamma        0.354    0.050    0.265     0.421      0.024    0.018       4.0     4.0       5.0      12.0   2.90
Is_begin   974.650  553.456  146.294  1881.653    245.128  184.346       5.0     5.0       5.0      12.0   2.45
Ia_begin  1569.941  812.071  505.606  2956.088    330.276  245.837       6.0     6.0       7.0      12.0   1.75
E_begin    876.178  784.738    8.663  2338.239    354.427  267.254       5.0     5.0       5.0      13.0   2.77