0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 114 log-likelihood matrix

         Estimate       SE
elpd_loo -1448.25   146.34
p_loo      152.85        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      111   97.4%
 (0.5, 0.7]   (ok)          3    2.6%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

               mean         sd  hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa            0.340      0.007   0.331      0.348      0.003     0.002       5.0     5.0       5.0      14.0   2.30
pu            0.894      0.007   0.881      0.900      0.003     0.003       4.0     4.0       5.0      13.0   3.10
mu            0.033      0.002   0.030      0.036      0.001     0.001       4.0     4.0       5.0      12.0   3.28
mus           0.384      0.070   0.262      0.429      0.033     0.025       4.0     4.0       5.0      12.0   3.19
gamma         0.896      0.059   0.807      0.968      0.028     0.021       4.0     4.0       5.0      31.0   2.95
Is_begin    988.707   1582.262  19.192   3769.803    758.735   577.270       4.0     4.0       5.0      24.0   3.15
Ia_begin    759.033   1238.947   0.470   2913.084    594.167   452.068       4.0     4.0       5.0      38.0   2.90
E_begin   10179.034  17416.972   1.348  41304.718   8346.108  6349.282       4.0     4.0       5.0      12.0   3.29