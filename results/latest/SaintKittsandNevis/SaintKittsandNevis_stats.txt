0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 139 log-likelihood matrix

         Estimate       SE
elpd_loo  -547.25    28.61
p_loo       18.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      138   99.3%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.7%

             mean       sd   hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.304    0.028    0.265     0.347      0.012    0.009       5.0     5.0       6.0      33.0   1.93
pu          0.864    0.006    0.852     0.874      0.002    0.002       7.0     7.0       8.0      16.0   1.57
mu          0.190    0.020    0.157     0.214      0.009    0.007       5.0     5.0       5.0      22.0   2.26
mus         0.246    0.018    0.219     0.273      0.006    0.005       8.0     8.0       7.0      21.0   1.66
gamma       0.547    0.040    0.462     0.619      0.014    0.010       8.0     8.0      10.0      16.0   1.39
Is_begin  133.516  183.658    2.478   533.354     81.705   61.482       5.0     5.0       5.0      13.0   2.80
Ia_begin  189.392  251.394    7.576   754.091    100.983   75.065       6.0     6.0       7.0      30.0   1.68
E_begin   512.184  335.245  141.340  1171.843    124.569   91.896       7.0     7.0       7.0      19.0   1.64