0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 178 log-likelihood matrix

         Estimate       SE
elpd_loo -1198.53    46.61
p_loo       29.20        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      175   98.3%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    2    1.1%

              mean        sd   hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.346     0.003    0.342      0.349      0.001     0.001       5.0     5.0       5.0      14.0   2.46
pu           0.898     0.002    0.896      0.900      0.001     0.001       4.0     4.0       5.0      12.0   3.21
mu           0.111     0.005    0.103      0.119      0.003     0.002       4.0     4.0       5.0      14.0   3.31
mus          0.168     0.037    0.105      0.211      0.017     0.013       4.0     4.0       5.0      12.0   4.07
gamma        0.399     0.088    0.244      0.510      0.042     0.032       4.0     4.0       5.0      12.0   3.26
Is_begin  1950.696  1107.136  339.747   3876.341    506.950   386.081       5.0     5.0       5.0      12.0   2.56
Ia_begin  5257.701  6242.367    1.702  17151.620   2934.694  2225.889       5.0     5.0       5.0      12.0   2.80
E_begin   3319.737  3904.825   58.996  10267.977   1842.433  1402.482       4.0     4.0       5.0      12.0   2.70