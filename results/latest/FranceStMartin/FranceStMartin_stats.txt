0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 56 log-likelihood matrix

         Estimate       SE
elpd_loo  -323.05    20.21
p_loo       14.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       54   96.4%
 (0.5, 0.7]   (ok)          1    1.8%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    1.8%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.260    0.055   0.186    0.349      0.023    0.017       6.0     6.0       7.0      26.0   1.71
pu          0.758    0.027   0.724    0.813      0.011    0.008       6.0     6.0       6.0      19.0   2.01
mu          0.116    0.009   0.101    0.133      0.004    0.003       5.0     5.0       5.0      16.0   2.80
mus         0.176    0.022   0.139    0.217      0.007    0.005       9.0     9.0      10.0      17.0   1.33
gamma       0.407    0.058   0.324    0.516      0.024    0.018       6.0     6.0       6.0      15.0   1.85
Is_begin  126.932  117.043  23.211  393.105     39.713   29.082       9.0     9.0       8.0      40.0   1.52
Ia_begin  433.069  321.352  23.780  921.118     96.016   69.744      11.0    11.0      10.0      24.0   1.37
E_begin   167.231  109.504  18.619  340.952     43.321   32.151       6.0     6.0       7.0      18.0   1.73