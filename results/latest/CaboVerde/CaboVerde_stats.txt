0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 177 log-likelihood matrix

         Estimate       SE
elpd_loo  -784.41    23.11
p_loo       13.99        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      173   97.7%
 (0.5, 0.7]   (ok)          2    1.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    1.1%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.268    0.039   0.207    0.322      0.019    0.014       4.0     4.0       5.0      17.0   3.25
pu          0.795    0.004   0.792    0.802      0.002    0.002       4.0     4.0       6.0      19.0   1.89
mu          0.390    0.004   0.385    0.396      0.002    0.001       5.0     5.0       5.0      13.0   3.01
mus         0.413    0.024   0.367    0.432      0.012    0.009       4.0     4.0       5.0      15.0   2.94
gamma       0.603    0.008   0.585    0.615      0.004    0.003       5.0     5.0       5.0      20.0   2.55
Is_begin   82.565   51.038   0.930  143.553     23.414   17.694       5.0     5.0       6.0      31.0   1.93
Ia_begin  214.838  238.916   7.674  644.327    113.764   86.458       4.0     4.0       5.0      12.0   3.52
E_begin    50.485   70.741   0.970  199.441     32.519   24.582       5.0     5.0       5.0      14.0   2.76