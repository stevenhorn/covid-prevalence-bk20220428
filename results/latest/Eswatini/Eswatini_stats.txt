0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 168 log-likelihood matrix

         Estimate       SE
elpd_loo  -831.48    22.62
p_loo       19.00        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      167   99.4%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    0.6%

              mean        sd    hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.195     0.019     0.168      0.223      0.009     0.007       5.0     5.0       5.0      14.0   2.87
pu           0.756     0.020     0.726      0.783      0.009     0.007       5.0     5.0       5.0      12.0   2.94
mu           0.229     0.010     0.211      0.245      0.005     0.004       5.0     5.0       5.0      22.0   3.16
mus          0.274     0.034     0.220      0.312      0.016     0.012       4.0     4.0       5.0      20.0   2.70
gamma        0.552     0.063     0.480      0.655      0.029     0.022       5.0     5.0       5.0      17.0   2.42
Is_begin  1040.213  1330.319    30.807   3660.757    459.582   337.009       8.0     8.0       5.0      13.0   2.38
Ia_begin  3617.374  2453.327   729.549   7769.535    882.251   649.017       8.0     8.0       6.0      19.0   1.96
E_begin   6150.899  3206.536  1292.358  10176.259   1383.950  1037.319       5.0     5.0       6.0      28.0   2.02