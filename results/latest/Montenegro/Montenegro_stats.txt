0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 160 log-likelihood matrix

         Estimate       SE
elpd_loo -1043.74    21.15
p_loo       17.91        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      159   99.4%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.188   0.018   0.164    0.218      0.008    0.006       5.0     5.0       5.0      12.0   2.83
pu         0.754   0.010   0.737    0.763      0.005    0.004       4.0     4.0       5.0      15.0   2.76
mu         0.106   0.001   0.104    0.108      0.001    0.001       4.0     4.0       5.0      13.0   2.96
mus        0.347   0.046   0.271    0.398      0.022    0.017       4.0     4.0       5.0      14.0   3.45
gamma      0.822   0.168   0.639    1.080      0.080    0.061       4.0     4.0       5.0      13.0   3.22
Is_begin   4.718   2.956   0.301    8.873      1.377    1.043       5.0     5.0       5.0      14.0   2.77
Ia_begin  68.505  85.880   2.595  215.426     41.166   31.328       4.0     4.0       5.0      12.0   3.03
E_begin   25.292  18.900   2.601   51.195      8.921    6.770       4.0     4.0       5.0      21.0   2.77