0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 94 log-likelihood matrix

         Estimate       SE
elpd_loo  -542.45    28.64
p_loo       16.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       93   98.9%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    1.1%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.272    0.043   0.183    0.324      0.018    0.014       6.0     6.0       6.0      14.0   1.91
pu          0.881    0.010   0.865    0.897      0.003    0.002       9.0     9.0       8.0      53.0   1.44
mu          0.134    0.008   0.120    0.148      0.003    0.002       6.0     6.0       6.0      12.0   2.07
mus         0.242    0.025   0.206    0.293      0.010    0.008       6.0     6.0       6.0      13.0   1.82
gamma       0.463    0.039   0.384    0.529      0.012    0.009      11.0    11.0      11.0      64.0   1.30
Is_begin   63.173   86.059   0.658  255.445     26.942   19.624      10.0    10.0      11.0      34.0   1.32
Ia_begin  216.794  325.903   3.934  809.942    128.458   95.300       6.0     6.0       5.0      15.0   2.37
E_begin   201.183  144.169  11.484  511.116     56.328   41.752       7.0     7.0       7.0      18.0   1.64