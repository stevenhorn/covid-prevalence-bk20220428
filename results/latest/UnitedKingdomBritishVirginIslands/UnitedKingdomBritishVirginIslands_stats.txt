0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 80 log-likelihood matrix

         Estimate       SE
elpd_loo  -362.46    26.03
p_loo       14.48        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       76   95.0%
 (0.5, 0.7]   (ok)          3    3.8%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    1    1.2%

             mean       sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.275    0.048   0.182     0.340      0.013    0.009      14.0    14.0      13.0      51.0   1.26
pu          0.812    0.018   0.783     0.848      0.005    0.004      13.0    13.0      14.0      33.0   1.23
mu          0.127    0.024   0.093     0.168      0.011    0.008       5.0     5.0       5.0      13.0   2.83
mus         0.162    0.030   0.119     0.225      0.013    0.009       6.0     6.0       6.0      17.0   2.14
gamma       0.382    0.040   0.296     0.458      0.013    0.010       9.0     9.0       8.0      19.0   1.48
Is_begin  458.624  314.991  10.334   914.861    113.412   83.439       8.0     8.0       8.0      80.0   1.48
Ia_begin  636.092  389.759  82.142  1323.093    129.745   94.884       9.0     9.0       8.0      24.0   1.48
E_begin   440.269  422.895  12.977  1243.749    158.154  116.740       7.0     7.0       6.0      19.0   1.89