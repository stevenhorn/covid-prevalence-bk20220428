0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 163 log-likelihood matrix

         Estimate       SE
elpd_loo  -862.41    20.12
p_loo       16.86        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      158   96.9%
 (0.5, 0.7]   (ok)          4    2.5%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    0    0.0%

              mean        sd  hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.210     0.033   0.180      0.270      0.016     0.012       4.0     4.0       5.0      23.0   2.65
pu           0.733     0.005   0.726      0.742      0.002     0.002       5.0     5.0       5.0      21.0   2.47
mu           0.232     0.006   0.222      0.240      0.003     0.002       4.0     4.0       5.0      12.0   3.95
mus          0.316     0.017   0.294      0.344      0.008     0.006       4.0     4.0       5.0      13.0   3.49
gamma        0.733     0.031   0.681      0.778      0.015     0.011       4.0     4.0       5.0      12.0   2.77
Is_begin  1404.016  1112.964  16.548   3196.817    520.955   394.868       5.0     5.0       5.0      15.0   2.97
Ia_begin  3053.645  3876.603   1.419  10366.328   1814.542  1375.367       5.0     5.0       5.0      29.0   3.08
E_begin    390.794   608.321   0.056   1582.924    288.884   219.454       4.0     4.0       5.0      13.0   3.30