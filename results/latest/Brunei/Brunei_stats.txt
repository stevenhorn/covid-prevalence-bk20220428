0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 117 log-likelihood matrix

         Estimate       SE
elpd_loo  -726.55    23.27
p_loo       19.92        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      115   98.3%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    1.7%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.183    0.022   0.164    0.220      0.010    0.008       4.0     4.0       5.0      17.0   2.33
pu          0.709    0.002   0.707    0.713      0.001    0.001       4.0     4.0       5.0      13.0   2.51
mu          0.168    0.009   0.156    0.180      0.004    0.003       4.0     4.0       5.0      18.0   3.44
mus         0.190    0.017   0.169    0.216      0.008    0.006       4.0     4.0       5.0      16.0   3.57
gamma       0.293    0.035   0.259    0.353      0.017    0.013       4.0     4.0       5.0      31.0   2.79
Is_begin   83.728  137.208   4.474  390.142     65.229   49.561       4.0     4.0       5.0      12.0   4.10
Ia_begin  170.866  162.891  36.972  484.430     77.109   58.548       4.0     4.0       5.0      17.0   2.62
E_begin   153.880   80.629   8.397  247.195     37.537   28.429       5.0     5.0       5.0      13.0   2.70