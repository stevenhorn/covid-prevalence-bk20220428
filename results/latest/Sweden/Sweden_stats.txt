0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 66 log-likelihood matrix

         Estimate       SE
elpd_loo  -616.00    16.94
p_loo       21.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       61   92.4%
 (0.5, 0.7]   (ok)          2    3.0%
   (0.7, 1]   (bad)         2    3.0%
   (1, Inf)   (very bad)    1    1.5%

              mean        sd  hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.280     0.029   0.225      0.308      0.014     0.010       4.0     4.0       5.0      12.0   3.22
pu           0.799     0.013   0.782      0.815      0.006     0.005       4.0     4.0       5.0      14.0   2.66
mu           0.142     0.003   0.138      0.147      0.001     0.001       4.0     4.0       5.0      14.0   3.90
mus          0.279     0.044   0.212      0.339      0.021     0.016       4.0     4.0       5.0      13.0   3.34
gamma        0.451     0.058   0.363      0.509      0.027     0.021       4.0     4.0       5.0      38.0   2.58
Is_begin  1084.384  1721.748   0.027   4134.546    782.762   590.784       5.0     5.0       5.0      15.0   3.34
Ia_begin  7485.194  8652.632   4.710  22927.776   4049.051  3068.938       5.0     5.0       5.0      12.0   2.44
E_begin   4108.652  6439.407   8.694  18055.846   2885.536  2173.461       5.0     5.0       5.0      12.0   3.34