0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 34 log-likelihood matrix

         Estimate       SE
elpd_loo  -217.36     9.91
p_loo        5.32        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       32   94.1%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         2    5.9%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.245    0.060   0.170     0.343      0.029    0.022       4.0     4.0       5.0      13.0   2.78
pu          0.779    0.049   0.725     0.872      0.023    0.018       5.0     5.0       5.0      13.0   3.46
mu          0.142    0.018   0.116     0.170      0.008    0.006       5.0     5.0       5.0      25.0   2.32
mus         0.202    0.022   0.167     0.244      0.009    0.007       6.0     6.0       7.0      12.0   1.85
gamma       0.372    0.058   0.239     0.431      0.026    0.020       5.0     5.0       6.0      18.0   2.10
Is_begin  173.948  185.059  18.352   539.444     73.236   54.354       6.0     6.0       6.0      19.0   1.77
Ia_begin  297.377  213.315  19.687   619.981     93.918   70.575       5.0     5.0       5.0      25.0   2.33
E_begin   572.224  322.578  53.057  1177.016    122.658   90.678       7.0     7.0       7.0      27.0   1.73