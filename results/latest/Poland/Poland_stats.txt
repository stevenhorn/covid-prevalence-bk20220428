0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 85 log-likelihood matrix

         Estimate       SE
elpd_loo  -826.96    14.00
p_loo       20.61        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       82   96.5%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         2    2.4%
   (1, Inf)   (very bad)    1    1.2%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.283   0.045   0.223    0.341      0.021    0.016       4.0     4.0       5.0      13.0   3.21
pu         0.877   0.003   0.873    0.882      0.001    0.001       4.0     4.0       5.0      16.0   2.97
mu         0.093   0.005   0.087    0.099      0.002    0.002       4.0     4.0       5.0      12.0   3.46
mus        0.205   0.021   0.181    0.231      0.010    0.008       4.0     4.0       5.0      12.0   2.77
gamma      0.506   0.029   0.456    0.544      0.014    0.011       4.0     4.0       5.0      12.0   3.42
Is_begin  14.913  11.541   0.518   34.870      5.468    4.153       4.0     4.0       5.0      50.0   2.90
Ia_begin  20.044  12.792   1.346   40.173      6.012    4.559       5.0     5.0       5.0      12.0   3.08
E_begin   27.269  13.986   4.874   49.575      5.344    4.087       7.0     6.0       8.0      19.0   1.53