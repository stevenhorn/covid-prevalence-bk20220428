0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 135 log-likelihood matrix

         Estimate       SE
elpd_loo  -794.72    16.13
p_loo       17.57        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      135  100.0%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

              mean        sd    hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.256     0.041     0.199      0.303      0.020     0.015       4.0     4.0       5.0      13.0   3.42
pu           0.884     0.008     0.876      0.896      0.004     0.003       4.0     4.0       5.0      12.0   2.96
mu           0.180     0.007     0.174      0.194      0.004     0.003       4.0     4.0       5.0      12.0   3.80
mus          0.098     0.022     0.074      0.134      0.011     0.008       4.0     4.0       5.0      24.0   3.00
gamma        0.460     0.050     0.386      0.530      0.024     0.018       4.0     4.0       5.0      15.0   3.45
Is_begin   484.036   745.709    44.684   1837.381    357.386   271.886       4.0     4.0       5.0      12.0   3.67
Ia_begin  2102.930  1990.893   171.407   5273.406    954.541   726.228       4.0     4.0       5.0      14.0   3.62
E_begin   6951.550  4616.349  1529.126  13256.817   2212.231  1682.963       4.0     4.0       5.0      13.0   3.49