0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 178 log-likelihood matrix

         Estimate       SE
elpd_loo    14.02    49.07
p_loo       32.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      171   96.1%
 (0.5, 0.7]   (ok)          3    1.7%
   (0.7, 1]   (bad)         2    1.1%
   (1, Inf)   (very bad)    2    1.1%

           mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.269   0.053   0.171    0.342      0.020    0.015       7.0     7.0       8.0      21.0   1.57
pu        0.865   0.026   0.812    0.894      0.010    0.008       7.0     7.0       8.0      17.0   1.51
mu        0.124   0.025   0.088    0.173      0.010    0.008       6.0     6.0       7.0      12.0   1.69
mus       0.147   0.027   0.106    0.201      0.008    0.006      11.0    11.0      11.0      63.0   1.32
gamma     0.338   0.072   0.238    0.521      0.027    0.020       7.0     7.0       7.0      17.0   1.69
Is_begin  0.853   0.759   0.082    1.862      0.207    0.150      13.0    13.0      12.0      39.0   1.27
Ia_begin  9.025  10.146   0.069   29.233      2.832    2.050      13.0    13.0       7.0      17.0   1.69
E_begin   4.646   4.639   0.047   13.802      1.497    1.093      10.0    10.0       6.0      11.0   1.83