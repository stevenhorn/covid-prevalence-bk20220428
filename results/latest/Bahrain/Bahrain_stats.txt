0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 178 log-likelihood matrix

         Estimate       SE
elpd_loo -1044.61    21.61
p_loo       14.37        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      177   99.4%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    0    0.0%

              mean        sd    hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.235     0.034     0.186      0.274      0.016     0.012       4.0     4.0       5.0      18.0   3.85
pu           0.766     0.009     0.755      0.779      0.004     0.003       4.0     4.0       5.0      12.0   3.51
mu           0.132     0.003     0.129      0.136      0.001     0.001       4.0     4.0       5.0      32.0   2.96
mus          0.148     0.015     0.129      0.170      0.007     0.005       4.0     4.0       5.0      16.0   3.34
gamma        0.424     0.052     0.336      0.477      0.025     0.019       4.0     4.0       5.0      12.0   4.31
Is_begin   451.671   246.702    42.554    690.964    115.179    87.268       5.0     5.0       6.0      26.0   2.13
Ia_begin  5813.157  4370.328  1294.231  14270.691   2087.260  1589.303       4.0     4.0       5.0      15.0   3.16
E_begin   1110.379  1554.462     3.303   4223.971    733.594   556.740       4.0     4.0       5.0      15.0   3.47