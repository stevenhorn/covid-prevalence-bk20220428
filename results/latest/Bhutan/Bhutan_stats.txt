0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 117 log-likelihood matrix

         Estimate       SE
elpd_loo  -513.74    22.29
p_loo       11.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      115   98.3%
 (0.5, 0.7]   (ok)          1    0.9%
   (0.7, 1]   (bad)         1    0.9%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.252   0.033   0.196    0.310      0.015    0.011       5.0     5.0       5.0      12.0   2.58
pu         0.868   0.009   0.854    0.881      0.004    0.003       5.0     5.0       5.0      12.0   3.29
mu         0.109   0.003   0.106    0.114      0.001    0.001       5.0     5.0       5.0      13.0   2.24
mus        0.173   0.027   0.135    0.215      0.013    0.010       4.0     4.0       5.0      12.0   3.28
gamma      0.319   0.017   0.292    0.348      0.007    0.005       5.0     5.0       6.0      21.0   1.95
Is_begin  15.302  15.398   0.186   44.276      4.839    3.526      10.0    10.0       8.0      25.0   1.56
Ia_begin  62.952  59.386   0.405  157.954     22.438   16.578       7.0     7.0       6.0      29.0   1.96
E_begin   30.299  33.533   2.449  102.699      9.875    7.167      12.0    12.0       8.0      28.0   1.50