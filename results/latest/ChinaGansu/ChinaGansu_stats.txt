0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 179 log-likelihood matrix

         Estimate       SE
elpd_loo  -434.16    46.73
p_loo       26.24        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      177   98.9%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    1    0.6%

           mean     sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa        0.239  0.040   0.151    0.289      0.017    0.013       5.0     5.0       6.0      13.0   1.79
pu        0.736  0.009   0.722    0.748      0.003    0.003       7.0     7.0       6.0      23.0   1.85
mu        0.159  0.010   0.142    0.181      0.005    0.003       5.0     5.0       5.0      15.0   2.32
mus       0.224  0.038   0.180    0.293      0.018    0.013       5.0     5.0       5.0      23.0   2.75
gamma     0.568  0.139   0.372    0.820      0.064    0.049       5.0     5.0       6.0      13.0   2.25
Is_begin  5.380  4.545   0.100   14.270      1.910    1.427       6.0     6.0       6.0      16.0   2.23
Ia_begin  6.755  6.374   0.497   19.124      2.407    1.778       7.0     7.0       8.0      14.0   1.57
E_begin   5.816  5.908   0.269   18.145      2.459    1.835       6.0     6.0       5.0      12.0   2.46