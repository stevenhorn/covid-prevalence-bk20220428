0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 107 log-likelihood matrix

         Estimate       SE
elpd_loo -1103.56    25.02
p_loo       21.39        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      103   96.3%
 (0.5, 0.7]   (ok)          1    0.9%
   (0.7, 1]   (bad)         3    2.8%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.195   0.008   0.184    0.208      0.004    0.003       5.0     5.0       5.0      13.0   2.84
pu         0.712   0.006   0.706    0.723      0.003    0.002       4.0     4.0       5.0      13.0   2.85
mu         0.071   0.001   0.070    0.072      0.000    0.000       4.0     4.0       5.0      12.0   3.31
mus        0.570   0.014   0.548    0.591      0.006    0.005       4.0     4.0       5.0      14.0   3.30
gamma      0.978   0.033   0.927    1.022      0.016    0.012       4.0     4.0       5.0      12.0   3.27
Is_begin   6.809   6.772   0.015   19.825      3.208    2.436       4.0     4.0       5.0      14.0   3.81
Ia_begin  25.761  27.938   0.090   77.979     13.288   10.097       4.0     4.0       5.0      21.0   3.11
E_begin   17.511   5.580  10.606   26.295      2.636    2.000       4.0     4.0       5.0      16.0   2.89