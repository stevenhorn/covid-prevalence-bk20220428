0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 114 log-likelihood matrix

         Estimate       SE
elpd_loo  -565.46    41.82
p_loo       22.11        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      110   96.5%
 (0.5, 0.7]   (ok)          1    0.9%
   (0.7, 1]   (bad)         2    1.8%
   (1, Inf)   (very bad)    1    0.9%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.276   0.046   0.195    0.340      0.021    0.016       5.0     5.0       5.0      18.0   2.79
pu         0.868   0.014   0.843    0.889      0.006    0.005       5.0     5.0       5.0      13.0   2.76
mu         0.128   0.005   0.120    0.134      0.002    0.002       5.0     5.0       6.0      14.0   1.97
mus        0.178   0.035   0.119    0.232      0.017    0.013       4.0     4.0       5.0      15.0   3.81
gamma      0.391   0.040   0.317    0.445      0.019    0.014       5.0     5.0       5.0      16.0   3.10
Is_begin  23.583  17.461   2.399   55.102      6.871    5.097       6.0     6.0       6.0      17.0   1.96
Ia_begin  58.028  60.209   1.346  162.825     25.135   18.764       6.0     6.0       6.0      21.0   2.19
E_begin   51.331  52.423   0.433  158.963     22.409   16.776       5.0     5.0       5.0      20.0   2.57