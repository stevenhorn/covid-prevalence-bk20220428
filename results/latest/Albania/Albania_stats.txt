0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 118 log-likelihood matrix

         Estimate       SE
elpd_loo  -888.73    29.75
p_loo       28.15        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      114   96.6%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         2    1.7%
   (1, Inf)   (very bad)    0    0.0%

               mean          sd   hdi_3%     hdi_97%  mcse_mean    mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa            0.346       0.004    0.337       0.350      0.002      0.001       5.0     5.0       5.0      14.0   2.43
pu            0.898       0.001    0.897       0.899      0.000      0.000       5.0     5.0       6.0      27.0   1.91
mu            0.122       0.015    0.106       0.142      0.007      0.005       4.0     4.0       6.0      27.0   2.27
mus           0.201       0.076    0.123       0.315      0.037      0.028       4.0     4.0       5.0      15.0   3.24
gamma         0.359       0.076    0.239       0.523      0.034      0.026       5.0     5.0       5.0      12.0   2.81
Is_begin  12519.891   10687.898  863.698   35737.445   4558.378   3411.645       5.0     5.0       5.0      14.0   2.45
Ia_begin  96313.830  149485.427  156.273  363595.551  71546.111  54418.021       4.0     4.0       5.0      14.0   2.59
E_begin   10912.557    7168.453   43.032   22680.769   3257.677   2458.564       5.0     5.0       5.0      12.0   2.56