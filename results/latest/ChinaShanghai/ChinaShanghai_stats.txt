0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 198 log-likelihood matrix

         Estimate       SE
elpd_loo  -588.15    15.70
p_loo       15.35        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      197   99.5%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.5%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.260    0.055   0.172    0.348      0.005    0.004     127.0   121.0     132.0     128.0   1.03
pu          0.834    0.054   0.736    0.900      0.016    0.012      12.0    11.0       9.0      12.0   1.44
mu          0.110    0.011   0.094    0.134      0.004    0.003       9.0     9.0       8.0      21.0   1.47
mus         0.196    0.034   0.134    0.258      0.009    0.007      14.0    14.0      16.0      51.0   1.21
gamma       0.434    0.085   0.284    0.589      0.014    0.010      37.0    37.0      38.0     194.0   1.11
Is_begin   65.205   42.360   2.110  132.835      3.825    2.711     123.0   123.0     106.0     113.0   1.04
Ia_begin  339.930  347.777   1.054  990.584     45.961   33.106      57.0    56.0      84.0      92.0   1.07
E_begin   176.113  154.713   0.435  461.740     16.207   11.497      91.0    91.0      72.0     108.0   1.05