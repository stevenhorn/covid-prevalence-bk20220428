0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 114 log-likelihood matrix

         Estimate       SE
elpd_loo  -893.90    15.75
p_loo       32.51        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      114  100.0%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

              mean         sd  hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.313      0.019   0.281      0.333      0.009     0.007       4.0     4.0       5.0      13.0   3.40
pu           0.882      0.005   0.878      0.890      0.002     0.002       4.0     4.0       5.0      25.0   2.86
mu           0.132      0.004   0.127      0.138      0.002     0.001       4.0     4.0       5.0      12.0   3.43
mus          0.283      0.026   0.242      0.317      0.012     0.009       4.0     4.0       5.0      12.0   3.45
gamma        0.535      0.150   0.376      0.709      0.072     0.055       4.0     4.0       5.0      12.0   2.93
Is_begin   442.577    591.992   0.003   1457.651    283.290   215.817       4.0     4.0       5.0      12.0   3.32
Ia_begin  3440.884   2574.211  61.026   7546.287   1156.432   871.356       5.0     5.0       5.0      16.0   2.39
E_begin   8009.631  13140.706  54.817  31477.971   6298.236  4791.525       4.0     4.0       5.0      18.0   3.06