0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 42 log-likelihood matrix

         Estimate       SE
elpd_loo  -302.38    10.40
p_loo        5.69        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       39   92.9%
 (0.5, 0.7]   (ok)          3    7.1%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.218    0.038   0.164     0.270      0.016    0.012       5.0     5.0       6.0      15.0   1.95
pu          0.863    0.027   0.808     0.891      0.011    0.008       6.0     6.0       5.0      21.0   2.41
mu          0.122    0.010   0.108     0.135      0.004    0.003       5.0     5.0       5.0      26.0   2.35
mus         0.165    0.009   0.153     0.184      0.003    0.003       6.0     6.0       6.0      17.0   1.88
gamma       0.261    0.038   0.208     0.370      0.016    0.012       6.0     5.0       6.0      12.0   1.86
Is_begin  535.500  417.068  15.403  1226.586    182.704  137.203       5.0     5.0       5.0      29.0   2.32
Ia_begin  889.718  511.809  61.946  1737.936    209.939  156.413       6.0     6.0       6.0      12.0   1.89
E_begin   783.437  696.794  10.706  2037.160    279.941  208.095       6.0     6.0       6.0      12.0   2.14