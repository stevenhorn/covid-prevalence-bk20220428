0 Divergences
Passed validation
2020-04-15 date of last case count
2022-04-21 date of last test positivity data
covid_prevalence version: WGM 0.1.7 2022-04-13

Data provenance: R = region/public health unit, P = province/state
week_start case_data_origin positivity_data_origin
2020-03-19         R                   P
2020-03-26         R                   P
2020-04-02         R                   P
2020-04-09         R                   P
