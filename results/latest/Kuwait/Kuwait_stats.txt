0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 174 log-likelihood matrix

         Estimate       SE
elpd_loo  -985.56    22.90
p_loo       15.90        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      172   98.9%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    1    0.6%

                mean         sd     hdi_3%     hdi_97%  mcse_mean    mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa             0.339      0.009      0.323       0.349      0.004      0.003       4.0     4.0       5.0      13.0   3.77
pu             0.898      0.001      0.897       0.899      0.000      0.000       4.0     4.0       5.0      14.0   3.63
mu             0.139      0.004      0.134       0.145      0.002      0.001       4.0     4.0       5.0      12.0   3.23
mus            0.160      0.032      0.112       0.196      0.015      0.012       4.0     4.0       5.0      20.0   2.71
gamma          0.441      0.008      0.429       0.456      0.004      0.003       5.0     5.0       5.0      13.0   3.16
Is_begin     128.122    158.027     12.231     442.391     74.306     56.361       5.0     5.0       5.0      13.0   3.06
Ia_begin  100679.298  63180.857  26382.664  219919.444  29409.252  22272.640       5.0     5.0       5.0      39.0   2.91
E_begin    11572.289  10664.615     87.423   31479.861   4911.777   3714.038       5.0     5.0       5.0      28.0   2.81