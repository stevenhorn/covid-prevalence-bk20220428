0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 131 log-likelihood matrix

         Estimate       SE
elpd_loo -1169.75    34.77
p_loo       31.89        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      129   98.5%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    1.5%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.199    0.019   0.176    0.225      0.009    0.007       4.0     4.0       5.0      16.0   3.31
pu          0.727    0.007   0.716    0.734      0.003    0.002       4.0     4.0       5.0      23.0   3.15
mu          0.120    0.001   0.119    0.121      0.000    0.000       4.0     4.0       5.0      14.0   2.32
mus         0.508    0.021   0.483    0.544      0.010    0.008       4.0     4.0       5.0      12.0   2.86
gamma       0.919    0.091   0.787    1.039      0.044    0.033       4.0     4.0       5.0      12.0   3.34
Is_begin    9.517   11.972   1.561   31.086      5.740    4.367       4.0     4.0       5.0      12.0   3.00
Ia_begin   11.942   10.849   1.084   30.049      5.202    3.958       4.0     4.0       5.0      12.0   3.78
E_begin   137.093  141.838   1.297  363.630     67.941   51.682       4.0     4.0       5.0      12.0   3.72