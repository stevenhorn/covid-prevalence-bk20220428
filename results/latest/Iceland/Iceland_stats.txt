0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 116 log-likelihood matrix

         Estimate       SE
elpd_loo  -812.27    20.84
p_loo        7.53        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      113   97.4%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         1    0.9%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.159   0.007   0.151    0.172      0.002    0.002       9.0     9.0      11.0      43.0   1.35
pu         0.704   0.003   0.701    0.709      0.001    0.001       6.0     6.0       7.0      14.0   1.78
mu         0.100   0.012   0.086    0.123      0.006    0.004       5.0     5.0       5.0      15.0   2.69
mus        0.146   0.011   0.126    0.165      0.004    0.003       7.0     7.0       7.0      36.0   1.65
gamma      0.338   0.067   0.243    0.424      0.031    0.024       5.0     5.0       6.0      17.0   2.11
Is_begin  17.293  20.992   0.123   60.305      6.508    4.737      10.0    10.0       7.0      53.0   1.70
Ia_begin  75.021  76.526  11.141  248.761     32.627   24.418       6.0     6.0       7.0      20.0   1.77
E_begin   41.777  43.533   0.632  138.678     14.412   10.536       9.0     9.0       8.0      20.0   1.54