0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 97 log-likelihood matrix

         Estimate       SE
elpd_loo  -983.71    22.66
p_loo       26.67        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       91   93.8%
 (0.5, 0.7]   (ok)          3    3.1%
   (0.7, 1]   (bad)         3    3.1%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.155   0.002   0.152    0.159      0.001    0.001       4.0     4.0       5.0      17.0   3.24
pu         0.702   0.001   0.700    0.704      0.001    0.000       5.0     5.0       5.0      13.0   3.14
mu         0.149   0.004   0.142    0.153      0.002    0.001       4.0     4.0       5.0      21.0   3.37
mus        0.324   0.021   0.288    0.353      0.010    0.008       4.0     4.0       5.0      14.0   4.30
gamma      0.689   0.031   0.644    0.736      0.015    0.011       4.0     4.0       5.0      12.0   3.20
Is_begin  13.646  15.568   1.358   43.898      7.420    5.640       4.0     4.0       5.0      12.0   2.61
Ia_begin  23.175  28.975   0.134   80.710     13.673   10.377       4.0     4.0       5.0      13.0   2.98
E_begin   24.841  17.992   5.027   63.662      8.052    6.064       5.0     5.0       5.0      13.0   2.54