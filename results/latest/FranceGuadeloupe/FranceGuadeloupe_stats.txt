0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 44 log-likelihood matrix

         Estimate       SE
elpd_loo  -365.82    20.82
p_loo       13.87        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       41   93.2%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         2    4.5%
   (1, Inf)   (very bad)    1    2.3%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.181    0.034   0.150    0.252      0.016    0.012       5.0     5.0       5.0      12.0   3.54
pu          0.715    0.010   0.702    0.733      0.005    0.004       5.0     5.0       5.0      17.0   2.96
mu          0.175    0.006   0.165    0.183      0.003    0.002       5.0     5.0       5.0      15.0   2.53
mus         0.280    0.021   0.251    0.309      0.010    0.008       4.0     4.0       5.0      15.0   3.52
gamma       0.508    0.093   0.404    0.665      0.044    0.034       4.0     4.0       5.0      17.0   2.32
Is_begin   36.467   43.981   0.114  121.661     20.720   15.720       5.0     5.0       5.0      15.0   3.32
Ia_begin  104.867  125.544  15.432  437.974     56.678   42.735       5.0     5.0       6.0      12.0   2.14
E_begin    49.919   31.177   6.884   97.557     13.271    9.930       6.0     6.0       6.0      13.0   2.04