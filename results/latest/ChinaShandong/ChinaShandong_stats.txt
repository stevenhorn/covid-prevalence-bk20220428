0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 109 log-likelihood matrix

         Estimate       SE
elpd_loo  -349.70    29.03
p_loo       15.64        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      100   91.7%
 (0.5, 0.7]   (ok)          5    4.6%
   (0.7, 1]   (bad)         3    2.8%
   (1, Inf)   (very bad)    1    0.9%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.290   0.026   0.250    0.330      0.011    0.008       5.0     5.0       6.0      20.0   1.84
pu         0.871   0.007   0.861    0.883      0.003    0.002       7.0     7.0       7.0      16.0   1.62
mu         0.109   0.006   0.098    0.117      0.003    0.002       4.0     4.0       5.0      14.0   3.68
mus        0.146   0.023   0.117    0.189      0.011    0.008       5.0     5.0       5.0      23.0   3.08
gamma      0.342   0.027   0.300    0.377      0.012    0.009       5.0     5.0       5.0      15.0   3.01
Is_begin  10.306   9.308   0.691   24.517      4.012    3.007       5.0     5.0       5.0      12.0   2.62
Ia_begin  81.034  48.345   9.582  154.854     21.166   15.894       5.0     5.0       5.0      14.0   2.23
E_begin   20.062  15.137   1.161   44.574      5.571    4.107       7.0     7.0       6.0      13.0   2.01