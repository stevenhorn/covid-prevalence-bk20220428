0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 120 log-likelihood matrix

         Estimate       SE
elpd_loo -1215.53    38.85
p_loo       23.30        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      117   97.5%
 (0.5, 0.7]   (ok)          2    1.7%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.152    0.002   0.150    0.156      0.001    0.001       5.0     5.0       5.0      12.0   2.36
pu          0.702    0.001   0.701    0.704      0.001    0.001       4.0     4.0       5.0      15.0   2.28
mu          0.100    0.004   0.092    0.104      0.002    0.001       5.0     5.0       5.0      12.0   3.40
mus         0.152    0.011   0.135    0.163      0.005    0.004       4.0     4.0       5.0      20.0   2.86
gamma       0.424    0.098   0.242    0.536      0.047    0.036       4.0     4.0       5.0      12.0   3.85
Is_begin  107.306  144.891   0.698  443.423     65.519   49.412       5.0     5.0       5.0      18.0   3.35
Ia_begin  188.076  263.275   4.529  691.863    125.105   95.047       4.0     4.0       5.0      28.0   2.32
E_begin    29.845   44.088   0.466  102.420     14.939   10.939       9.0     9.0       6.0      22.0   2.16