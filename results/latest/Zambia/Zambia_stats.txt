0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 146 log-likelihood matrix

         Estimate       SE
elpd_loo  -820.74    21.30
p_loo       15.55        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      144   98.6%
 (0.5, 0.7]   (ok)          0    0.0%
   (0.7, 1]   (bad)         1    0.7%
   (1, Inf)   (very bad)    1    0.7%

              mean        sd   hdi_3%    hdi_97%  mcse_mean   mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.260     0.036    0.197      0.300      0.017     0.013       4.0     4.0       5.0      17.0   2.95
pu           0.769     0.037    0.733      0.828      0.018     0.014       4.0     4.0       5.0      14.0   3.97
mu           0.144     0.008    0.131      0.156      0.004     0.003       4.0     4.0       5.0      13.0   3.46
mus          0.278     0.057    0.222      0.376      0.027     0.021       4.0     4.0       5.0      18.0   3.16
gamma        0.532     0.068    0.425      0.619      0.033     0.025       4.0     4.0       5.0      12.0   3.66
Is_begin  1180.568   860.345  198.540   2705.360    394.027   297.701       5.0     5.0       5.0      13.0   2.63
Ia_begin  6426.509  5012.437  116.124  14409.079   2387.691  1814.713       4.0     4.0       5.0      17.0   3.04
E_begin   4886.227  5142.818  199.718  14630.547   2419.271  1835.129       5.0     5.0       5.0      12.0   2.98