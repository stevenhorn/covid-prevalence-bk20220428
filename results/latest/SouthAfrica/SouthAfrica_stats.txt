0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 98 log-likelihood matrix

         Estimate       SE
elpd_loo  -930.71    30.29
p_loo       32.59        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       93   94.9%
 (0.5, 0.7]   (ok)          4    4.1%
   (0.7, 1]   (bad)         1    1.0%
   (1, Inf)   (very bad)    0    0.0%

               mean         sd  hdi_3%    hdi_97%  mcse_mean    mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa            0.279      0.046   0.209      0.338      0.021      0.016       5.0     5.0       5.0      38.0   2.36
pu            0.884      0.012   0.866      0.900      0.006      0.004       4.0     4.0       5.0      17.0   3.18
mu            0.151      0.004   0.145      0.155      0.002      0.001       4.0     4.0       5.0      15.0   3.18
mus           0.428      0.076   0.332      0.545      0.036      0.028       4.0     4.0       5.0      14.0   3.17
gamma         0.859      0.070   0.739      0.919      0.033      0.025       4.0     4.0       5.0      25.0   2.39
Is_begin   2138.329   3615.321  17.653   9327.571   1730.354   1316.109       4.0     4.0       5.0      12.0   3.81
Ia_begin    501.995    676.030   7.757   1740.241    323.474    246.025       4.0     4.0       5.0      13.0   2.98
E_begin   19726.875  34066.242  43.518  83431.590  16312.476  12408.233       4.0     4.0       5.0      16.0   2.76