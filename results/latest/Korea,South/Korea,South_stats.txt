0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 116 log-likelihood matrix

         Estimate       SE
elpd_loo -1159.13    28.31
p_loo       37.26        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      112   96.6%
 (0.5, 0.7]   (ok)          3    2.6%
   (0.7, 1]   (bad)         1    0.9%
   (1, Inf)   (very bad)    0    0.0%

             mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.165    0.006   0.157    0.172      0.003    0.002       4.0     4.0       5.0      12.0   2.67
pu          0.724    0.009   0.710    0.737      0.004    0.003       4.0     4.0       5.0      13.0   3.95
mu          0.134    0.002   0.132    0.137      0.001    0.001       4.0     4.0       5.0      16.0   3.85
mus         0.142    0.007   0.131    0.149      0.003    0.002       4.0     4.0       5.0      15.0   3.15
gamma       0.504    0.041   0.439    0.550      0.020    0.015       4.0     4.0       5.0      12.0   3.43
Is_begin    3.825    2.526   0.187    7.709      1.167    0.883       5.0     5.0       5.0      20.0   2.77
Ia_begin  208.681  371.761   0.909  904.756    166.134  125.090       5.0     5.0       5.0      12.0   3.33
E_begin    11.828   11.643   0.921   33.580      5.533    4.204       4.0     4.0       5.0      15.0   2.73