0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -287.22    11.04
p_loo        5.08        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      118   95.2%
 (0.5, 0.7]   (ok)          4    3.2%
   (0.7, 1]   (bad)         1    0.8%
   (1, Inf)   (very bad)    1    0.8%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.223   0.026   0.181    0.260      0.011    0.009       5.0     5.0       5.0      16.0   2.59
pu         0.793   0.021   0.758    0.821      0.010    0.008       5.0     5.0       5.0      17.0   3.05
mu         0.108   0.012   0.090    0.129      0.006    0.004       4.0     4.0       5.0      14.0   2.94
mus        0.160   0.048   0.107    0.251      0.023    0.017       4.0     4.0       5.0      25.0   2.54
gamma      0.327   0.062   0.197    0.398      0.028    0.021       5.0     5.0       5.0      12.0   2.41
Is_begin   9.948   5.884   2.749   20.522      2.307    1.710       7.0     7.0       6.0      33.0   1.89
Ia_begin  21.958  26.286   3.767   80.197     12.508    9.505       4.0     4.0       6.0      12.0   1.87
E_begin    9.474   8.846   0.751   25.768      3.039    2.227       8.0     8.0       7.0      15.0   1.73