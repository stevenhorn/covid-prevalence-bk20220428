0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 124 log-likelihood matrix

         Estimate       SE
elpd_loo  -797.43    35.77
p_loo       10.07        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      122   98.4%
 (0.5, 0.7]   (ok)          2    1.6%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.290   0.031   0.242    0.339      0.014    0.011       5.0     5.0       5.0      23.0   2.54
pu         0.782   0.014   0.763    0.810      0.006    0.005       5.0     5.0       5.0      12.0   2.87
mu         0.104   0.017   0.087    0.141      0.008    0.006       4.0     4.0       5.0      13.0   3.27
mus        0.026   0.001   0.024    0.028      0.000    0.000       6.0     6.0       7.0      15.0   1.70
gamma      0.389   0.032   0.341    0.437      0.015    0.011       4.0     4.0       5.0      12.0   3.95
Is_begin   1.328   1.767   0.029    5.127      0.683    0.506       7.0     7.0       6.0      15.0   2.19
Ia_begin  39.211  42.452   0.356  102.822     17.127   12.737       6.0     6.0       5.0      25.0   2.42
E_begin   12.771   5.986   2.301   23.710      2.201    1.622       7.0     7.0       7.0      18.0   1.66