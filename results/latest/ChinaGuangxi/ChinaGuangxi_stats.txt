0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 157 log-likelihood matrix

         Estimate       SE
elpd_loo  -497.50    20.04
p_loo        7.07        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      153   97.5%
 (0.5, 0.7]   (ok)          2    1.3%
   (0.7, 1]   (bad)         1    0.6%
   (1, Inf)   (very bad)    1    0.6%

             mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.278   0.017   0.246    0.303      0.007    0.005       7.0     7.0       7.0      38.0   1.61
pu          0.793   0.028   0.752    0.827      0.013    0.010       4.0     4.0       5.0      12.0   3.07
mu          0.111   0.003   0.108    0.118      0.001    0.001       5.0     5.0       5.0      12.0   3.13
mus         0.188   0.036   0.148    0.258      0.017    0.013       5.0     5.0       5.0      15.0   2.85
gamma       0.368   0.049   0.300    0.453      0.023    0.017       5.0     5.0       5.0      13.0   2.65
Is_begin   14.539   6.660   2.371   25.259      2.283    1.673       9.0     9.0       8.0      12.0   1.57
Ia_begin  105.830  82.577  15.702  233.333     30.517   22.502       7.0     7.0       7.0      20.0   1.79
E_begin    15.888  20.369   0.420   52.890      7.771    5.747       7.0     7.0       6.0      17.0   1.92