0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 125 log-likelihood matrix

         Estimate       SE
elpd_loo  -751.17    18.07
p_loo       15.24        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      122   97.6%
 (0.5, 0.7]   (ok)          3    2.4%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.213   0.048   0.152    0.281      0.023    0.018       4.0     4.0       5.0      14.0   3.88
pu         0.733   0.016   0.718    0.759      0.007    0.006       4.0     4.0       5.0      14.0   2.94
mu         0.177   0.013   0.158    0.192      0.006    0.005       4.0     4.0       5.0      12.0   3.14
mus        0.219   0.036   0.163    0.266      0.017    0.013       4.0     4.0       5.0      27.0   2.99
gamma      0.395   0.050   0.318    0.457      0.024    0.018       4.0     4.0       5.0      16.0   3.57
Is_begin  45.970  62.021   4.105  175.076     29.428   22.352       4.0     4.0       5.0      12.0   3.06
Ia_begin  55.513  32.659   4.774   97.471     15.423   11.706       4.0     4.0       5.0      23.0   2.67
E_begin   17.321  11.297   0.993   32.312      5.393    4.100       4.0     4.0       5.0      15.0   3.56