0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 92 log-likelihood matrix

         Estimate       SE
elpd_loo  -557.73    19.03
p_loo       12.38        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       88   95.7%
 (0.5, 0.7]   (ok)          1    1.1%
   (0.7, 1]   (bad)         1    1.1%
   (1, Inf)   (very bad)    2    2.2%

             mean       sd  hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa          0.226    0.040   0.159     0.293      0.014    0.010       8.0     8.0       8.0      18.0   1.60
pu          0.748    0.029   0.710     0.796      0.014    0.010       4.0     4.0       5.0      13.0   2.93
mu          0.250    0.011   0.236     0.267      0.005    0.004       4.0     4.0       5.0      17.0   3.27
mus         0.297    0.017   0.275     0.328      0.008    0.006       5.0     5.0       5.0      12.0   2.68
gamma       0.706    0.054   0.633     0.805      0.024    0.018       5.0     5.0       5.0      12.0   2.28
Is_begin  565.212  769.911  41.255  2061.301    367.700  279.577       4.0     4.0       5.0      12.0   3.87
Ia_begin   89.359  133.740   2.597   334.470     63.850   48.545       4.0     4.0       5.0      20.0   3.17
E_begin   498.524  776.507   9.634  1874.214    371.973  282.962       4.0     4.0       5.0      14.0   3.11