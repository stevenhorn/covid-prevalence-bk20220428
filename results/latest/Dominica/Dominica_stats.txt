0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 75 log-likelihood matrix

         Estimate       SE
elpd_loo  -406.18     7.97
p_loo        7.68        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       72   96.0%
 (0.5, 0.7]   (ok)          3    4.0%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

              mean       sd   hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.278    0.047    0.191     0.342      0.021    0.016       5.0     5.0       5.0      18.0   2.72
pu           0.766    0.024    0.728     0.801      0.011    0.008       5.0     5.0       5.0      19.0   3.21
mu           0.129    0.008    0.114     0.142      0.004    0.003       5.0     5.0       6.0      15.0   2.36
mus          0.157    0.023    0.134     0.206      0.011    0.008       5.0     5.0       6.0      16.0   2.04
gamma        0.343    0.099    0.194     0.497      0.047    0.036       4.0     4.0       5.0      30.0   3.08
Is_begin   611.983  326.010   58.327  1072.343    147.432  111.191       5.0     5.0       6.0      34.0   2.06
Ia_begin   673.128  690.327   51.814  2056.765    315.588  238.375       5.0     5.0       6.0      15.0   2.04
E_begin   1070.591  624.413  254.917  2061.987    257.580  199.242       6.0     6.0       7.0      14.0   1.77