0 Divergences 
Failed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 78 log-likelihood matrix

         Estimate       SE
elpd_loo  -697.76    11.98
p_loo       16.01        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)       72   92.3%
 (0.5, 0.7]   (ok)          2    2.6%
   (0.7, 1]   (bad)         2    2.6%
   (1, Inf)   (very bad)    2    2.6%

            mean      sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.298   0.016   0.268    0.329      0.007    0.005       6.0     6.0       6.0      20.0   2.08
pu         0.835   0.004   0.828    0.842      0.002    0.001       6.0     6.0       6.0      28.0   1.88
mu         0.104   0.006   0.093    0.111      0.003    0.002       4.0     4.0       5.0      12.0   3.46
mus        0.205   0.035   0.152    0.266      0.017    0.013       4.0     4.0       5.0      12.0   4.42
gamma      0.463   0.018   0.431    0.487      0.008    0.006       5.0     5.0       5.0      21.0   3.15
Is_begin  83.787  79.643  15.707  228.227     37.575   28.515       4.0     4.0       5.0      19.0   2.68
Ia_begin  31.952  37.396   0.942   79.593     12.874    9.438       8.0     8.0       6.0      12.0   1.85
E_begin   48.957  23.087  16.666   80.721      8.175    6.006       8.0     8.0       6.0      13.0   1.89