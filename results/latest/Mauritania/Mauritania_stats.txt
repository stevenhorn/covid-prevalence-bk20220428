0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 180 log-likelihood matrix

         Estimate       SE
elpd_loo  -869.88    28.47
p_loo       37.97        -

There has been a warning during the calculation. Please check the results.
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      175   97.2%
 (0.5, 0.7]   (ok)          3    1.7%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    2    1.1%

              mean        sd   hdi_3%   hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa           0.233     0.054    0.173     0.329      0.026    0.019       4.0     4.0       5.0      15.0   3.43
pu           0.731     0.012    0.716     0.748      0.005    0.004       5.0     5.0       5.0      17.0   2.76
mu           0.202     0.005    0.196     0.209      0.002    0.002       4.0     4.0       5.0      26.0   2.95
mus          0.298     0.040    0.247     0.371      0.019    0.014       5.0     5.0       5.0      12.0   3.32
gamma        0.539     0.086    0.396     0.662      0.041    0.031       4.0     4.0       5.0      13.0   2.80
Is_begin   790.437   545.835  123.472  1809.623    251.644  190.308       5.0     5.0       5.0      13.0   3.15
Ia_begin  2799.937  2001.330   60.797  5645.375    921.507  696.771       5.0     5.0       5.0      12.0   2.70
E_begin   2008.282  1817.486   61.596  5569.041    837.632  633.436       5.0     5.0       6.0      12.0   2.02