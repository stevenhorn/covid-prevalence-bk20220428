0 Divergences 
Passed validation 
2022-03-21 date of last case count
covid_prevalence version: 0.3.0+unobserved_days_dev_2022_02_28

Computed from 400 by 175 log-likelihood matrix

         Estimate       SE
elpd_loo  -434.03    12.41
p_loo       11.51        -
------

Pareto k diagnostic values:
                         Count   Pct.
(-Inf, 0.5]   (good)      172   98.3%
 (0.5, 0.7]   (ok)          3    1.7%
   (0.7, 1]   (bad)         0    0.0%
   (1, Inf)   (very bad)    0    0.0%

            mean       sd  hdi_3%  hdi_97%  mcse_mean  mcse_sd  ess_mean  ess_sd  ess_bulk  ess_tail  r_hat
pa         0.184    0.024   0.153    0.225      0.009    0.007       6.0     6.0       6.0      12.0   2.02
pu         0.728    0.013   0.711    0.756      0.004    0.003      10.0    10.0       8.0      21.0   1.58
mu         0.120    0.004   0.112    0.124      0.002    0.001       4.0     4.0       6.0      14.0   2.07
mus        0.254    0.014   0.234    0.278      0.006    0.005       5.0     5.0       6.0      41.0   1.94
gamma      0.448    0.048   0.384    0.520      0.022    0.017       5.0     5.0       6.0      33.0   2.20
Is_begin  52.755   38.682   5.068  124.091     14.626   10.807       7.0     7.0       6.0      17.0   1.76
Ia_begin  74.940  111.462   0.209  342.913     49.785   37.483       5.0     5.0       6.0      12.0   1.95
E_begin   61.568   49.105   2.648  157.950     21.217   15.905       5.0     5.0       5.0      14.0   2.40